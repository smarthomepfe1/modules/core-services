package com.asm.smarthome.coreservices.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface DeviceRepository extends MongoRepository<Device,String> {
    Optional<Device> findByRef(String ref);
}
