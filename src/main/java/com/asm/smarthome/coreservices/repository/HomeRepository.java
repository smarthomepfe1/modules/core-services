package com.asm.smarthome.coreservices.repository;


import com.asm.smarthome.coremodels.model.Home;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HomeRepository extends MongoRepository<Home,String> {
}
